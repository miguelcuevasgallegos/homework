# Descripción del Proyecto:

Nombre del Proyecto: Anthos Academy Homework

* The original package name 'com. anthosacademyhomework. anthos academy homework' is invalid and this project uses 'com.anthosacademyhomework.anthos.academy.homework' instead.

# Tecnologías Utilizadas:

* Spring Boot: Para facilitar la configuración, creación del microservicio y desarrollo del proyecto.
* Spring Data JPA: Para interactuar con la base de datos MySQL.
* Clean Arquitecture y Patrones de diseño.
* Paradigma Orientado Objetos, encapsulamiento, herencia si es que aplica.
* Bucles, Listas, Mapas o Arreglos.
* Exceptiones genericas o personalizadas para controlar los errores si el que el servicio no responde.
* Utilizar API REST.
* Crear repositorio en GitLab y almacenar proyecto utilizando GIT.
* Swagger: Agregar swagger al proyecto.
* MySQL: Como base de datos para almacenar las tareas.
* RestTemplate: Para consumir servicios externos.

# Buenas Practicas:

* Utilizar buenas practicas en general en el proyecto, escritura de nombres de metodos, package, etc.
* Clean Arquitecture utilización de usecase, repository y datasource.
* Mensajes de commit en GIT.
* Nombres en la url de los endpoints


# Funcionalidades del Proyecto:

### Gestión de Tareas:

* Crear, obtener, actualizar y eliminar tareas.
* Almacenar información como título, descripción, fecha de vencimiento, valor UF, valor dolar, valor euro, valor utm y valor bitcoin. (valores consumir API externa)
* Listar feriados en chile consumiendo API externa
* Listar farmacias del País solo Arica, Talca, Concepción, Los Angeles, Puerto montt y Aysen.
* Listar farmacias de turno solo chiguayante, curanilahue, las cabras, rio negro y tocopilla.
* Listar indicadores economicos mas las tareas.

# Api Externas

* [API indicadores ecónomicos](https://mindicador.cl/)
* [API feriados /holidays.json](https://www.feriadosapp.com/api/)
* [API farmacias del país y turno](https://datos.gob.cl/dataset/farmacias-en-chile/)


# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.1.6/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.1.6/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.1.6/reference/htmlsingle/index.html#web)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.1.6/reference/htmlsingle/index.html#data.sql.jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

