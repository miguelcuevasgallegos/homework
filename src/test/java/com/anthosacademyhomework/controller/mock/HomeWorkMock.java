package com.anthosacademyhomework.controller.mock;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;

import java.util.ArrayList;
import java.util.List;

public class HomeWorkMock {
    public static HomeWork taskStandard(){
        HomeWork task1 = new HomeWork(01L, "Crear test de clases", "Poner a prueba con el test unitario", "24/12/2023");
        return task1;
    }
    public static List<HomeWork> taskList(){
        List<HomeWork> list1 = new ArrayList<>();
        HomeWork task1 = new HomeWork(01L, "Crear test de clases", "Poner a prueba con el test unitario", "24/12/2023");
        HomeWork task3 = new HomeWork(03L, "Veritficar datos", "Necesitas verificar cada dato de manera unitaria", "28/12/2023");
        list1.add(task1);
        list1.add(task3);
        return list1;
    }
}
