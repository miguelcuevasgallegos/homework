package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.controller.mock.HomeWorkMock;
import com.anthosacademyhomework.resources.datasource.task.HomeWorkDAO;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.usecase.createhomework.CreateHomeWorkUseCase;
import com.anthosacademyhomework.usecase.getallhomework.GetAllHomeWorkUseCase;
import com.anthosacademyhomework.usecase.gethomework.GetHomeWorkUseCase;
import com.anthosacademyhomework.usecase.updatehomework.UpdateHomeWorkUseCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertWith;
import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class HomeWorkControllerTest {
    HomeWorkController homeWorkController;
    @Mock
    CreateHomeWorkUseCase createHomeWorkUseCase;
    @Mock
    GetAllHomeWorkUseCase getAllHomeWorkUseCase;
    @Mock
    UpdateHomeWorkUseCase updateHomeWorkUseCase;
    @Mock
    GetHomeWorkUseCase getHomeWorkUseCase;
    @BeforeEach
    void setUp() {
        homeWorkController = new HomeWorkController(createHomeWorkUseCase, getAllHomeWorkUseCase, getHomeWorkUseCase, updateHomeWorkUseCase);
    }

    @Test
    void When_CreateOneTask_Expect_ExecuteCorrectly() {
        //Mockito.when(createHomeWorkUseCase.createHWUC());
        HomeWork task2 = HomeWorkMock.taskStandard();
        homeWorkController.createTask(task2);
        Mockito.verify(createHomeWorkUseCase, Mockito.times(1)).createHWUC(task2);
    }

    @Test
    void Should_ExpectedAllTask_When_StateUnderTest() {
        Mockito.when(getAllHomeWorkUseCase.getAllHW()).thenReturn(HomeWorkMock.taskList());
        List<HomeWork> list = homeWorkController.listall();
        Assertions.assertNotNull(list);
    }

    @Test
    void gethomework() {
        // creación de variables de datos falsos
        Long id = 01L;

        Mockito.when(getHomeWorkUseCase.gettask(Mockito.anyLong())).thenReturn(Optional.of(HomeWorkMock.taskStandard()));
        //llamada de método principal

        Optional<HomeWork> response = homeWorkController.gethomework(id);
        Assertions.assertNotNull(response);
        assertThat(response.get().getId()).isEqualTo(01L);

    }
//TODO: Should_ExpectedBehavior_When_StateUnderTest
//When_StateUnderTest_Expect_ExpectedBehavior
//Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior
    @Test
    void updatehomework() {
    }
}