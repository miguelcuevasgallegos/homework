package com.anthosacademyhomework.usecase.indicatoreconomicmorehw;

import com.anthosacademyhomework.resources.datasource.economicsvalues.model.IndicadorEcoModel;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.getallhomework.GetAllHomeWorkRepository;
import com.anthosacademyhomework.resources.repository.getindicators.GetIndicatorsRepository;
import com.anthosacademyhomework.usecase.indicatoreconomicmorehw.mapper.IndicatorEconomicTaskToIndicatorEcoTaskModelMapper;
import com.anthosacademyhomework.usecase.indicatoreconomicmorehw.model.IndicatorEcoTaskModel;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndicatorEconomicMoreHWUseCase {
    private final GetIndicatorsRepository getIndicatorsRepository;
    private final GetAllHomeWorkRepository getHomeWorkRepository;
    private final IndicatorEconomicTaskToIndicatorEcoTaskModelMapper mapUseCase;

    public IndicatorEconomicMoreHWUseCase(GetIndicatorsRepository getIndicatorsRepository, GetAllHomeWorkRepository getHomeWorkRepository, IndicatorEconomicTaskToIndicatorEcoTaskModelMapper mapUseCase) {
        this.getIndicatorsRepository = getIndicatorsRepository;
        this.getHomeWorkRepository = getHomeWorkRepository;
        this.mapUseCase = mapUseCase;
    }

    public IndicatorEcoTaskModel indicatorEcoTaskModel(){
        IndicadorEcoModel infoEco = getIndicatorsRepository.indicatorEcoModel();
        List<HomeWork> list = getHomeWorkRepository.getAllHomeWork();

        return mapUseCase.mapper(infoEco, list);
    }
}
