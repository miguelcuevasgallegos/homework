package com.anthosacademyhomework.usecase.indicatoreconomicmorehw.mapper;

import com.anthosacademyhomework.resources.datasource.economicsvalues.model.IndicadorEcoModel;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.usecase.indicatoreconomicmorehw.model.IndicatorEcoTaskModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IndicatorEconomicTaskToIndicatorEcoTaskModelMapper {

    public IndicatorEcoTaskModel mapper(IndicadorEcoModel iem, List<HomeWork> listhw){
        //TODO: revisar creación de tareas para que no falle el mapper
        return IndicatorEcoTaskModel.builder()
                .withHomeWorkList(listhw)
                .withIndicadorEcoModel(iem)
                .build();
    }
}
