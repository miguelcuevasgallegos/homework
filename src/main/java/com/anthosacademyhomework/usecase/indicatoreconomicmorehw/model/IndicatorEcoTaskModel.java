package com.anthosacademyhomework.usecase.indicatoreconomicmorehw.model;

import com.anthosacademyhomework.resources.datasource.economicsvalues.model.IndicadorEcoModel;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;

import java.util.List;

public class IndicatorEcoTaskModel {

    private List<HomeWork> homeWorkList;

    private IndicadorEcoModel indicadorEcoModel;

    public List<HomeWork> getHomeWorkList() {
        return homeWorkList;
    }

    public IndicadorEcoModel getIndicadorEcoModel() {
        return indicadorEcoModel;
    }

    public static IndicatorEcoTaskModelBuilder builder() {
        return new IndicatorEcoTaskModelBuilder();
    }
    public static final class IndicatorEcoTaskModelBuilder {
        private List<HomeWork> homeWorkList;
        private IndicadorEcoModel indicadorEcoModel;

        private IndicatorEcoTaskModelBuilder() {
        }

        public IndicatorEcoTaskModelBuilder withHomeWorkList(List<HomeWork> homeWorkList) {
            this.homeWorkList = homeWorkList;
            return this;
        }

        public IndicatorEcoTaskModelBuilder withIndicadorEcoModel(IndicadorEcoModel indicadorEcoModel) {
            this.indicadorEcoModel = indicadorEcoModel;
            return this;
        }

        public IndicatorEcoTaskModel build() {
            IndicatorEcoTaskModel indicatorEcoTaskModel = new IndicatorEcoTaskModel();
            indicatorEcoTaskModel.indicadorEcoModel = this.indicadorEcoModel;
            indicatorEcoTaskModel.homeWorkList = this.homeWorkList;
            return indicatorEcoTaskModel;
        }
    }
}
