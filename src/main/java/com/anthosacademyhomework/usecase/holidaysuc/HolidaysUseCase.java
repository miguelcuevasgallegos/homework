package com.anthosacademyhomework.usecase.holidaysuc;

import com.anthosacademyhomework.resources.datasource.holidays.model.HolidaysModel;
import com.anthosacademyhomework.resources.repository.getholidays.GetHolidaysRepository;
import org.springframework.stereotype.Service;

@Service
public class HolidaysUseCase {
    private final GetHolidaysRepository getHolidaysRepository;

    public HolidaysUseCase(GetHolidaysRepository getHolidaysRepository) {
        this.getHolidaysRepository = getHolidaysRepository;
    }

    public HolidaysModel holidaysModel(){

        return getHolidaysRepository.getHolidaysModel();
    }

}
