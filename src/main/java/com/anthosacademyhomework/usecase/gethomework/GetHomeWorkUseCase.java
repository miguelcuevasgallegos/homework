package com.anthosacademyhomework.usecase.gethomework;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.gethomework.GetHomeWorkRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GetHomeWorkUseCase {
    private final GetHomeWorkRepository getHomeWorkId;

    public GetHomeWorkUseCase(GetHomeWorkRepository getHomeWorkId) {
        this.getHomeWorkId = getHomeWorkId;
    }

    public Optional<HomeWork> gettask(Long id){
        return getHomeWorkId.getHomeWork(id);
    }
}
