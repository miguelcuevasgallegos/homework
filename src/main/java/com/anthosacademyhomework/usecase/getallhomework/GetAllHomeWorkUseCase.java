package com.anthosacademyhomework.usecase.getallhomework;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.getallhomework.GetAllHomeWorkRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAllHomeWorkUseCase {
    private final GetAllHomeWorkRepository getAllHomeWorkRepository;

    public GetAllHomeWorkUseCase(GetAllHomeWorkRepository getAllHomeWorkRepository) {
        this.getAllHomeWorkRepository = getAllHomeWorkRepository;
    }

    public List<HomeWork> getAllHW(){
       return getAllHomeWorkRepository.getAllHomeWork();
  }
}
