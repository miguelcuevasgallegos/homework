package com.anthosacademyhomework.usecase.deletehomework;

import com.anthosacademyhomework.resources.repository.deletehomework.DeleteHomeWorkRepository;
import org.springframework.stereotype.Service;

@Service
public class DeleteHomeWorkUseCase {
    private final DeleteHomeWorkRepository deleteHomeWorkRepository;

    public DeleteHomeWorkUseCase(DeleteHomeWorkRepository deleteHomeWorkRepository) {
        this.deleteHomeWorkRepository = deleteHomeWorkRepository;
    }

    public void deleteHomeWork(Long id){
        deleteHomeWorkRepository.deletedHomeWork(id);
    }
}
