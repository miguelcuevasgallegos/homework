package com.anthosacademyhomework.usecase.createhomework;

import com.anthosacademyhomework.common.utils.HomeWorkCheck;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.createhomework.CreateHomeWorkRepository;
import org.springframework.stereotype.Service;

@Service
public class CreateHomeWorkUseCase {
    private final CreateHomeWorkRepository createHomeWorkRepository;

    public CreateHomeWorkUseCase(CreateHomeWorkRepository createHomeWorkRepository) {
        this.createHomeWorkRepository = createHomeWorkRepository;
    }

    public void createHWUC(HomeWork homeWork){
        if (HomeWorkCheck.validarFormato(homeWork.getTitle())){
            throw new RuntimeException("Debes título coherente");
        }
        createHomeWorkRepository.createHomeWork(homeWork);
    }
}
