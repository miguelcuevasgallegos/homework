package com.anthosacademyhomework.usecase.getfamaciesturn;

import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;
import com.anthosacademyhomework.resources.repository.getfarmacyturn.GetFarmacyTurnRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetFarmacyTurnUseCase {
    private final GetFarmacyTurnRepository getFarmacyTurnRepository;

    public GetFarmacyTurnUseCase(GetFarmacyTurnRepository getFarmacyTurnRepository) {
        this.getFarmacyTurnRepository = getFarmacyTurnRepository;
    }

    public List<DetalleFarmacyModel> farmacyTurnModel(){

        return getFarmacyTurnRepository.getFarmacyTurnModel()
                .stream().filter(x -> x.getComuna_nombre().equals("CHIGUAYANTE") || x
                        .getComuna_nombre().equals("CURANILAHUE") || x
                        .getComuna_nombre().equals("LAS CABRAS") || x
                        .getComuna_nombre().equals("TOCOPILLA")).collect(Collectors.toList());
    }
}
