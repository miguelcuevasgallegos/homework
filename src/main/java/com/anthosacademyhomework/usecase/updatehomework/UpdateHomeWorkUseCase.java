package com.anthosacademyhomework.usecase.updatehomework;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.updatehomework.UpdateHomeWorkRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UpdateHomeWorkUseCase {
    private final UpdateHomeWorkRepository updateHomeWorkRepository;

    public UpdateHomeWorkUseCase(UpdateHomeWorkRepository updateHomeWorkRepository) {
        this.updateHomeWorkRepository = updateHomeWorkRepository;
    }

    public Optional<HomeWork> getUpdateHomeWorkRepository(HomeWork homeWork, Long id) {
        return updateHomeWorkRepository.updateHomeWork(homeWork, id);
    }
}
