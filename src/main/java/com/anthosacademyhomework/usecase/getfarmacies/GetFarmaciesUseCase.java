package com.anthosacademyhomework.usecase.getfarmacies;

import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;
import com.anthosacademyhomework.resources.repository.getfarmacy.GetFarmacyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetFarmaciesUseCase {
    private final GetFarmacyRepository getFarmacyRepository;

    public GetFarmaciesUseCase(GetFarmacyRepository getFarmacyRepository) {
        this.getFarmacyRepository = getFarmacyRepository;
    }

    public List<DetalleFarmacyModel> farmacyModel(){

        return getFarmacyRepository.getFarmacyModel()
                .stream().filter(x -> x.getComuna_nombre().equals("TALCA") || x
                        .getComuna_nombre().equals("CONCEPCION")|| x
                        .getComuna_nombre().equals("LOS ANGELES")|| x
                        .getComuna_nombre().equals("PUERTO MONTT")|| x
                        .getComuna_nombre().equals("AYSEN")).collect(Collectors.toList());
    }
}
