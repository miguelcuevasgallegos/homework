package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.usecase.indicatoreconomicmorehw.IndicatorEconomicMoreHWUseCase;
import com.anthosacademyhomework.usecase.indicatoreconomicmorehw.model.IndicatorEcoTaskModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping ("/api")
public class EcoTaskController {
    private final IndicatorEconomicMoreHWUseCase indicatorEconomicMoreHWUseCase;

    public EcoTaskController(IndicatorEconomicMoreHWUseCase indicatorEconomicMoreHWUseCase) {
        this.indicatorEconomicMoreHWUseCase = indicatorEconomicMoreHWUseCase;
    }

    @GetMapping(path = "/task")
    public ResponseEntity<IndicatorEcoTaskModel> indicators (){
        return ResponseEntity.ok(indicatorEconomicMoreHWUseCase.indicatorEcoTaskModel());
    }
}
