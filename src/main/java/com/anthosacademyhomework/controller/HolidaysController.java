package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resources.datasource.holidays.model.HolidaysModel;
import com.anthosacademyhomework.usecase.holidaysuc.HolidaysUseCase;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class HolidaysController {
    private final HolidaysUseCase holidaysUseCase;

    public HolidaysController(HolidaysUseCase holidaysUseCase) {
        this.holidaysUseCase = holidaysUseCase;
    }

    @GetMapping(path = "/holidays")
    public HolidaysModel holidays (){
        return holidaysUseCase.holidaysModel();
    }
}
