package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.config.JWTAuthtenticationConfig;
import com.anthosacademyhomework.controller.model.UserModel;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class LoginController {
    private final JWTAuthtenticationConfig jwtAuthtenticationConfig;

    public LoginController(JWTAuthtenticationConfig jwtAuthtenticationConfig) {
        this.jwtAuthtenticationConfig = jwtAuthtenticationConfig;
    }

    @PostMapping("login")
    public UserModel login(
            @RequestParam("user") String username,
            @RequestParam("encryptedPass") String encryptedPass) {

        String token = jwtAuthtenticationConfig.getJWTToken(username);

        return UserModel.builder()
                .withUser(username)
                .withPass(encryptedPass)
                .withToken(token)
                .build();
    }
}

