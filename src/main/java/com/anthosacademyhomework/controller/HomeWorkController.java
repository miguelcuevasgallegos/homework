package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.usecase.createhomework.CreateHomeWorkUseCase;
import com.anthosacademyhomework.usecase.getallhomework.GetAllHomeWorkUseCase;
import com.anthosacademyhomework.usecase.gethomework.GetHomeWorkUseCase;
import com.anthosacademyhomework.usecase.updatehomework.UpdateHomeWorkUseCase;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class HomeWorkController {


    private final CreateHomeWorkUseCase createHomeWorkUseCase;
    private final GetAllHomeWorkUseCase getAllHomeWorkUseCase;
    private final GetHomeWorkUseCase getHomeWorkUseCase;
    private final UpdateHomeWorkUseCase updateHomeWorkUseCase;

    public HomeWorkController(CreateHomeWorkUseCase createHomeWorkUseCase, GetAllHomeWorkUseCase getAllHomeWorkUseCase, GetHomeWorkUseCase getHomeWorkUseCase, UpdateHomeWorkUseCase updateHomeWorkUseCase) {
        this.createHomeWorkUseCase = createHomeWorkUseCase;
        this.getAllHomeWorkUseCase = getAllHomeWorkUseCase;
        this.getHomeWorkUseCase = getHomeWorkUseCase;
        this.updateHomeWorkUseCase = updateHomeWorkUseCase;
    }

    @PostMapping(path = "/createtask")
    public void createTask(@RequestBody HomeWork homeWork) {
        createHomeWorkUseCase.createHWUC(homeWork);
    }

    @GetMapping(path = "/alltask")
    public List<HomeWork> listall() {
        return getAllHomeWorkUseCase.getAllHW();
    }

    @GetMapping(path = "/gettask/{id}")
    public Optional<HomeWork> gethomework(@PathVariable Long id) {
        return getHomeWorkUseCase.gettask(id);
    }

    @PutMapping(path = "/update/{id}")
    public Optional<HomeWork> updatehomework(@RequestBody HomeWork homeWork, @PathVariable Long id) {
        return updateHomeWorkUseCase.getUpdateHomeWorkRepository(homeWork, id);
    }

}
