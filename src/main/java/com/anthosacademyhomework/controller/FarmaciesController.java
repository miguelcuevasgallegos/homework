package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;
import com.anthosacademyhomework.usecase.getfamaciesturn.GetFarmacyTurnUseCase;
import com.anthosacademyhomework.usecase.getfarmacies.GetFarmaciesUseCase;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class FarmaciesController {
    private final GetFarmaciesUseCase getFarmaciesUseCase;
    private final GetFarmacyTurnUseCase getFarmacyTurnUseCase;

    public FarmaciesController(GetFarmaciesUseCase getFarmaciesUseCase, GetFarmacyTurnUseCase getFarmacyTurnUseCase) {
        this.getFarmaciesUseCase = getFarmaciesUseCase;
        this.getFarmacyTurnUseCase = getFarmacyTurnUseCase;
    }

    @GetMapping (path = "/farmacies")
    public List<DetalleFarmacyModel> farmacies(){
        return getFarmaciesUseCase.farmacyModel();
    }
    @GetMapping(path = "/farmacies/turn")
    public List<DetalleFarmacyModel> farmaciesTurn (){
        return getFarmacyTurnUseCase.farmacyTurnModel();
    }
}

