package com.anthosacademyhomework.resources.datasource.economicsvalues;

import com.anthosacademyhomework.resources.datasource.economicsvalues.model.IndicadorEcoModel;

public interface EconomicsValueDataSource {
    IndicadorEcoModel getAllIndicatorEconomics();
}
