package com.anthosacademyhomework.resources.datasource.economicsvalues.impl;

import com.anthosacademyhomework.resources.datasource.config.ApiConfig;
import com.anthosacademyhomework.resources.datasource.economicsvalues.EconomicsValueDataSource;
import com.anthosacademyhomework.resources.datasource.economicsvalues.model.IndicadorEcoModel;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

@Component
public class EconomicsValueDataSourceImpl implements EconomicsValueDataSource {
    private final ApiConfig apiConfig;
    String url = "https://www.mindicador.cl/api";

    public EconomicsValueDataSourceImpl(ApiConfig apiConfig) {
        this.apiConfig = apiConfig;
    }

    @Override
    public IndicadorEcoModel getAllIndicatorEconomics() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity requestHttpEntity = new HttpEntity(headers);
            ResponseEntity<IndicadorEcoModel> response = apiConfig.restTemplate().exchange(url, HttpMethod.GET, requestHttpEntity, IndicadorEcoModel.class);
            return response.getBody();
        } catch (Exception e) {
            //throw new ExceptionDataSource("no se ah encontrado el dato");
        } return null;
    }
}
