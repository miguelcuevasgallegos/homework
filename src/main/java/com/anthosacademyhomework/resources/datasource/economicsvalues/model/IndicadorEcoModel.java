package com.anthosacademyhomework.resources.datasource.economicsvalues.model;

public class IndicadorEcoModel {

    private String version;
    private String autor;
    private String fecha;

    private DetalleEcoModel uf;
    private DetalleEcoModel dolar;
    private DetalleEcoModel euro;
    private DetalleEcoModel utm;
    private DetalleEcoModel bitcoin;


    public DetalleEcoModel getDolar() {
        return dolar;
    }

    public void setDolar(DetalleEcoModel dolar) {
        this.dolar = dolar;
    }

    public DetalleEcoModel getEuro() {
        return euro;
    }

    public void setEuro(DetalleEcoModel euro) {
        this.euro = euro;
    }

    public DetalleEcoModel getUtm() {
        return utm;
    }

    public void setUtm(DetalleEcoModel utm) {
        this.utm = utm;
    }

    public DetalleEcoModel getBitcoin() {
        return bitcoin;
    }

    public void setBitcoin(DetalleEcoModel bitcoin) {
        this.bitcoin = bitcoin;
    }

    public DetalleEcoModel getUf() {
        return uf;
    }

    public void setUf(DetalleEcoModel uf) {
        this.uf = uf;
    }

    public IndicadorEcoModel() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
