package com.anthosacademyhomework.resources.datasource.economicsvalues;

public class Values {
    private double valueuf;
    private double valuedollar;
    private double valueeuro;
    private double valueutm;
    private double valuebitcoin;

    public Values(double valueuf, double valuedollar, double valueeuro, double valueutm, double valuebitcoin) {
        this.valueuf = valueuf;
        this.valuedollar = valuedollar;
        this.valueeuro = valueeuro;
        this.valueutm = valueutm;
        this.valuebitcoin = valuebitcoin;
    }

    public double getValueuf() {
        return valueuf;
    }

    public void setValueuf(double valueuf) {
        this.valueuf = valueuf;
    }

    public double getValuedollar() {
        return valuedollar;
    }

    public void setValuedollar(double valuedollar) {
        this.valuedollar = valuedollar;
    }

    public double getValueeuro() {
        return valueeuro;
    }

    public void setValueeuro(double valueeuro) {
        this.valueeuro = valueeuro;
    }

    public double getValueutm() {
        return valueutm;
    }

    public void setValueutm(double valueutm) {
        this.valueutm = valueutm;
    }

    public double getValuebitcoin() {
        return valuebitcoin;
    }

    public void setValuebitcoin(double valuebitcoin) {
        this.valuebitcoin = valuebitcoin;
    }
}
