package com.anthosacademyhomework.resources.datasource.farmacy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties (ignoreUnknown = true)
public class DetalleFarmacyModel implements Serializable {
    @JsonProperty ("fecha")
    private String fecha;
    @JsonProperty ("local_nombre")
    private String local_nombre;
    @JsonProperty ("comuna_nombre")
    private String comuna_nombre;
    @JsonProperty ("localidad_nombre")
    private String localidad_nombre;
    @JsonProperty ("local_direccion")
    private String local_direccion;
    @JsonProperty("funcionamiento_hora_apertura")
    private String funcionamiento_hora_apertura;
    @JsonProperty("funcionamiento_hora_cierre")
    private String funcionamiento_hora_cierre;

    public DetalleFarmacyModel() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLocal_nombre() {
        return local_nombre;
    }

    public void setLocal_nombre(String local_nombre) {
        this.local_nombre = local_nombre;
    }

    public String getComuna_nombre() {
        return comuna_nombre;
    }

    public void setComuna_nombre(String comuna_nombre) {
        this.comuna_nombre = comuna_nombre;
    }

    public String getLocalidad_nombre() {
        return localidad_nombre;
    }

    public void setLocalidad_nombre(String localidad_nombre) {
        this.localidad_nombre = localidad_nombre;
    }

    public String getLocal_direccion() {
        return local_direccion;
    }

    public void setLocal_direccion(String local_direccion) {
        this.local_direccion = local_direccion;
    }

    public String getFuncionamiento_hora_apertura() {
        return funcionamiento_hora_apertura;
    }

    public void setFuncionamiento_hora_apertura(String funcionamiento_hora_apertura) {
        this.funcionamiento_hora_apertura = funcionamiento_hora_apertura;
    }

    public String getFuncionamiento_hora_cierre() {
        return funcionamiento_hora_cierre;
    }

    public void setFuncionamiento_hora_cierre(String funcionamiento_hora_cierre) {
        this.funcionamiento_hora_cierre = funcionamiento_hora_cierre;
    }
}
