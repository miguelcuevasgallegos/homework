package com.anthosacademyhomework.resources.datasource.farmacy.impl;


import com.anthosacademyhomework.resources.datasource.config.ApiConfig;
import com.anthosacademyhomework.resources.datasource.farmacy.FarmacyDS;
import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FarmacyDSImpl implements FarmacyDS {
    private final ApiConfig apiConfig;
    private static final ObjectMapper objectMapper = new ObjectMapper();
    String url = "https://midas.minsal.cl/farmacia_v2/WS/getLocales.php";
    String link = "https://midas.minsal.cl/farmacia_v2/WS/getLocalesTurnos.php";

    public FarmacyDSImpl(ApiConfig apiConfig) {
        this.apiConfig = apiConfig;
    }

    @Override
    public List<DetalleFarmacyModel> getFarmacy() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity requestHttpEntity = new HttpEntity(headers);
            ResponseEntity<String> response = apiConfig.restTemplate().exchange(url, HttpMethod.GET, requestHttpEntity, String.class);
            List<DetalleFarmacyModel> detalle = objectMapper.readValue(response.getBody(), new TypeReference<List<DetalleFarmacyModel>>() {}
            );
            return detalle;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DetalleFarmacyModel> getFarmacyTurn() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity requestHttpEntity = new HttpEntity<>(headers);
            ResponseEntity<String> response = apiConfig.restTemplate().exchange(link, HttpMethod.GET, requestHttpEntity, String.class);
            List<DetalleFarmacyModel> change = objectMapper.readValue(response.getBody(), new TypeReference<List<DetalleFarmacyModel>>() {}
            );
            return change;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
