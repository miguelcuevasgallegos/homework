package com.anthosacademyhomework.resources.datasource.farmacy;

import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;

import java.util.List;

public interface FarmacyDS {
    List<DetalleFarmacyModel> getFarmacy();

    List<DetalleFarmacyModel> getFarmacyTurn();
}
