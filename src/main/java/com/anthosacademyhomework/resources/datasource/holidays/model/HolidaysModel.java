package com.anthosacademyhomework.resources.datasource.holidays.model;

import java.util.List;

public class HolidaysModel {
    private String status;
    private List<DetalleHolidaysModel> data;


    public HolidaysModel() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DetalleHolidaysModel> getData() {
        return data;
    }

    public void setData(List<DetalleHolidaysModel> data) {
        this.data = data;
    }
}
