package com.anthosacademyhomework.resources.datasource.holidays.model;

public class DetalleHolidaysModel {
    private String date;
    private  String title;
    private String type;
    private  boolean ilanielable;
    private  String extra;

    public DetalleHolidaysModel() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isIlanielable() {
        return ilanielable;
    }

    public void setIlanielable(boolean ilanielable) {
        this.ilanielable = ilanielable;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
