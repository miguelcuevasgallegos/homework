package com.anthosacademyhomework.resources.datasource.holidays;

import com.anthosacademyhomework.resources.datasource.holidays.model.HolidaysModel;

public interface HolidaysDataSource {
    HolidaysModel getHolidays();
}
