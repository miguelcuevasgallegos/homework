package com.anthosacademyhomework.resources.datasource.holidays.impl;

import com.anthosacademyhomework.resources.datasource.config.ApiConfig;
import com.anthosacademyhomework.resources.datasource.holidays.HolidaysDataSource;
import com.anthosacademyhomework.resources.datasource.holidays.model.HolidaysModel;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

@Component
public class HolidaysDataSourceImpl implements HolidaysDataSource {
    private final ApiConfig apiConfig;

    String url = "https://api.victorsanmartin.com/feriados/en.json";

    public HolidaysDataSourceImpl(ApiConfig apiConfig) {
        this.apiConfig = apiConfig;
    }

    @Override
    public HolidaysModel getHolidays() {
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity requestHttpEntity = new HttpEntity(headers);
            ResponseEntity<HolidaysModel> response = apiConfig.restTemplate().exchange(url, HttpMethod.GET, requestHttpEntity, HolidaysModel.class);
            return response.getBody();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
