package com.anthosacademyhomework.resources.datasource.task;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import org.springframework.data.repository.CrudRepository;

public interface HomeWorkDAO extends CrudRepository<HomeWork, Long> {
}
