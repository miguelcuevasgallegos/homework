package com.anthosacademyhomework.resources.datasource.task.entities;

import javax.persistence.*;

@Entity(name = "task_homework")
public class HomeWork {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private String dateexpiration;

    public HomeWork(Long id, String title, String description, String dateexpiration) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.dateexpiration = dateexpiration;
    }

    public HomeWork() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateexpiration() {
        return dateexpiration;
    }

    public void setDateexpiration(String dateexpiration) {
        this.dateexpiration = dateexpiration;
    }

    @Override
    public String toString() {
        return "HomeWork{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dateexpiration='" + dateexpiration + '\'' +
                '}';
    }
}
