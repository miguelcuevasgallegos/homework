package com.anthosacademyhomework.resources.repository.createhomework.impl;

import com.anthosacademyhomework.resources.datasource.task.HomeWorkDAO;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.createhomework.CreateHomeWorkRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CreateHomeWorkRepositoryImpl implements CreateHomeWorkRepository {
    private final HomeWorkDAO create;

    public CreateHomeWorkRepositoryImpl(HomeWorkDAO create) {
        this.create = create;
    }


    @Override
    public void createHomeWork(HomeWork createHomeWork) {create.save(createHomeWork);}
}
