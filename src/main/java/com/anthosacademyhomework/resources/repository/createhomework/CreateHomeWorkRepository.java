package com.anthosacademyhomework.resources.repository.createhomework;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;

public interface CreateHomeWorkRepository {
    public void createHomeWork (HomeWork createHomeWork);

}
