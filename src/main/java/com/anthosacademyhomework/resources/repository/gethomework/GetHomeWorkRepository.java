package com.anthosacademyhomework.resources.repository.gethomework;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;

import java.util.Optional;

public interface GetHomeWorkRepository {
    Optional<HomeWork> getHomeWork (Long id);
}
