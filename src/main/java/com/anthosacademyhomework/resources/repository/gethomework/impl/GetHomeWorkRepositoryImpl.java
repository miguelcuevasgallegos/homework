package com.anthosacademyhomework.resources.repository.gethomework.impl;

import com.anthosacademyhomework.resources.datasource.task.HomeWorkDAO;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.gethomework.GetHomeWorkRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class GetHomeWorkRepositoryImpl implements GetHomeWorkRepository {
    private final HomeWorkDAO get;
    public GetHomeWorkRepositoryImpl(HomeWorkDAO get) {
        this.get = get;
    }
    @Override
    public Optional<HomeWork> getHomeWork(Long id) {return get.findById(id);}
}
