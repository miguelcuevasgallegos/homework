package com.anthosacademyhomework.resources.repository.deletehomework;

public interface DeleteHomeWorkRepository {
    void deletedHomeWork (Long id);
}
