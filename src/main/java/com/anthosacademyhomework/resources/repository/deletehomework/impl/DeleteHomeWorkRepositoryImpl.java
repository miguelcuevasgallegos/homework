package com.anthosacademyhomework.resources.repository.deletehomework.impl;

import com.anthosacademyhomework.resources.datasource.task.HomeWorkDAO;
import com.anthosacademyhomework.resources.repository.deletehomework.DeleteHomeWorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DeleteHomeWorkRepositoryImpl implements DeleteHomeWorkRepository {
    @Autowired
    HomeWorkDAO delete;
    @Override
    public void deletedHomeWork(Long id) {delete.deleteById(id);}
}
