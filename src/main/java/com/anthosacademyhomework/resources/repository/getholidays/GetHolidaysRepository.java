package com.anthosacademyhomework.resources.repository.getholidays;

import com.anthosacademyhomework.resources.datasource.holidays.model.HolidaysModel;

public interface GetHolidaysRepository {
    HolidaysModel getHolidaysModel();
}
