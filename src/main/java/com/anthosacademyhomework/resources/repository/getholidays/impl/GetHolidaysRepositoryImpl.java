package com.anthosacademyhomework.resources.repository.getholidays.impl;

import com.anthosacademyhomework.resources.datasource.holidays.HolidaysDataSource;
import com.anthosacademyhomework.resources.datasource.holidays.model.HolidaysModel;
import com.anthosacademyhomework.resources.repository.getholidays.GetHolidaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GetHolidaysRepositoryImpl implements GetHolidaysRepository {
    @Autowired
    HolidaysDataSource holidaysDataSource;
    @Override
    public HolidaysModel getHolidaysModel() {
        return holidaysDataSource.getHolidays();
    }
}
