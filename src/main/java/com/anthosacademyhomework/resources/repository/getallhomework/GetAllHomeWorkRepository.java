package com.anthosacademyhomework.resources.repository.getallhomework;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;

import java.util.List;

public interface GetAllHomeWorkRepository {
    List<HomeWork> getAllHomeWork();
}
