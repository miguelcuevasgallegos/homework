package com.anthosacademyhomework.resources.repository.getallhomework.impl;

import com.anthosacademyhomework.resources.datasource.task.HomeWorkDAO;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.getallhomework.GetAllHomeWorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GetAllHomeWorkRepositoryImpl implements GetAllHomeWorkRepository {
    @Autowired
    HomeWorkDAO getAll;


    @Override
    public List<HomeWork> getAllHomeWork() {
        return (List<HomeWork>) getAll.findAll();
    }
}
