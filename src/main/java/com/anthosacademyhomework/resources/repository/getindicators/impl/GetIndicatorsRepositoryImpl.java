package com.anthosacademyhomework.resources.repository.getindicators.impl;

import com.anthosacademyhomework.resources.datasource.economicsvalues.EconomicsValueDataSource;
import com.anthosacademyhomework.resources.datasource.economicsvalues.model.IndicadorEcoModel;
import com.anthosacademyhomework.resources.repository.getindicators.GetIndicatorsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GetIndicatorsRepositoryImpl implements GetIndicatorsRepository {
    @Autowired
    EconomicsValueDataSource economicsValueDataSource;
    @Override
    public IndicadorEcoModel indicatorEcoModel() {
        return economicsValueDataSource.getAllIndicatorEconomics();
    }
}
