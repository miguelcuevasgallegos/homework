package com.anthosacademyhomework.resources.repository.getindicators;

import com.anthosacademyhomework.resources.datasource.economicsvalues.model.IndicadorEcoModel;

public interface GetIndicatorsRepository {
    IndicadorEcoModel indicatorEcoModel();

}
