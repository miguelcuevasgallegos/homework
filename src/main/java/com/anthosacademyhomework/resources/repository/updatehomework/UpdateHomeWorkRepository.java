package com.anthosacademyhomework.resources.repository.updatehomework;

import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;

import java.util.Optional;

public interface UpdateHomeWorkRepository {
    Optional<HomeWork> updateHomeWork(HomeWork homeWork, Long id) ;
}
