package com.anthosacademyhomework.resources.repository.updatehomework.impl;

import com.anthosacademyhomework.resources.datasource.task.HomeWorkDAO;
import com.anthosacademyhomework.resources.datasource.task.entities.HomeWork;
import com.anthosacademyhomework.resources.repository.updatehomework.UpdateHomeWorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UpdateHomeWorkRepositoryImpl implements UpdateHomeWorkRepository {
    @Autowired
    HomeWorkDAO update;

    @Override
    public Optional<HomeWork> updateHomeWork(HomeWork homeWork, Long id) {
        Optional<HomeWork> entity = update.findById(id);
        if (entity.isPresent()) {
            entity.get().setTitle(homeWork.getTitle());
            entity.get().setDescription(homeWork.getDescription());
            entity.get().setDateexpiration(homeWork.getDateexpiration());
            return Optional.of(update.save(entity.get()));
        } else {
            //TODO: agregar exeption controlada, task not found.
        }

        return entity;
    }
}




