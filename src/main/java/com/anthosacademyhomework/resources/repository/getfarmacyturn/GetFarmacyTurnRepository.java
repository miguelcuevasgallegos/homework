package com.anthosacademyhomework.resources.repository.getfarmacyturn;

import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;

import java.util.List;

public interface GetFarmacyTurnRepository {
    List<DetalleFarmacyModel> getFarmacyTurnModel();
}
