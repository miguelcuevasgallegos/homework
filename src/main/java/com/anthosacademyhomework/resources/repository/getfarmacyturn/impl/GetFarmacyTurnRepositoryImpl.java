package com.anthosacademyhomework.resources.repository.getfarmacyturn.impl;

import com.anthosacademyhomework.resources.datasource.farmacy.FarmacyDS;
import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;
import com.anthosacademyhomework.resources.repository.getfarmacyturn.GetFarmacyTurnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class GetFarmacyTurnRepositoryImpl implements GetFarmacyTurnRepository {
    @Autowired
    FarmacyDS farmacyDS;
    @Override
    public List<DetalleFarmacyModel> getFarmacyTurnModel() {
        return farmacyDS.getFarmacyTurn();
    }
}
