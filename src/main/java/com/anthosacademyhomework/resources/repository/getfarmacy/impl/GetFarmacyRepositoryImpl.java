package com.anthosacademyhomework.resources.repository.getfarmacy.impl;

import com.anthosacademyhomework.resources.datasource.farmacy.FarmacyDS;
import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;
import com.anthosacademyhomework.resources.repository.getfarmacy.GetFarmacyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GetFarmacyRepositoryImpl implements GetFarmacyRepository {
    @Autowired
    FarmacyDS farmacyDS;

    @Override
    public List<DetalleFarmacyModel> getFarmacyModel() {
        return farmacyDS.getFarmacy();
    }
}
