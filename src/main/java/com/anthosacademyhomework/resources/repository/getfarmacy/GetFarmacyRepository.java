package com.anthosacademyhomework.resources.repository.getfarmacy;

import com.anthosacademyhomework.resources.datasource.farmacy.model.DetalleFarmacyModel;

import java.util.List;

public interface GetFarmacyRepository {
    List<DetalleFarmacyModel> getFarmacyModel();
}
