package com.anthosacademyhomework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnthosAcademyHomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnthosAcademyHomeworkApplication.class, args);
	}

}
