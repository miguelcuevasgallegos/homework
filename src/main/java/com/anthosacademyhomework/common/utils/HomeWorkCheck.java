package com.anthosacademyhomework.common.utils;

import java.util.regex.Pattern;

public class HomeWorkCheck {
    public static boolean validatedDescription(String description){
        if(description.length() <= 10){
            return false;
        } else if (description.contains(".")) {
            return false;
        }
        return true;
    }
    // buscar condiciones especiales, expresiones regulares.

    public static boolean validarFormato(String input) {
        // Utilizando Pattern para realizar la validación con expresión regular
        return Pattern.matches("^[a-zA-Z0-9]+$", input);
    }

}


